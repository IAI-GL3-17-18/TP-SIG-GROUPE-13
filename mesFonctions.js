function initialize() {
		// création de la carte et paramétrage général : centre et niveau de zoom
		//var map = L.map('mapid').setView([30.375321, 69.34511599999996], 8);

		var map = L.map('mapid', {
			fullscreenControl: {
				pseudoFullscreen: false
			}
		  }).setView([0,0], 6);

		  var limite=L.geoJson(
			pk_limite
			).addTo(map);
		  map.fitBounds(limite.getBounds());
		  var osmGeocoder = new L.Control.OSMGeocoder({placeholder: 'Recherche...'});
		  map.addControl(osmGeocoder);
		  
		  //population

/*
		  var population=L.geoJson(
			pop
			).addTo(map).addTo(map).bindPopup(pop.X_LONGITUD).openPopup();
		  map.fitBounds(population.getBounds());*/


		  //coordonnee
		  L.control.coordinates().addTo(map);
		  L.control.ruler().addTo(map);

		  
				  


				 
 
		
		
		var satellite = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v10/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9nbml0ZXRlIiwiYSI6ImNqZHQ5N3NyOTBtbDEyd3Qzc21zbGczZGsifQ.fZl8Ld3MTbGPMlG-bB-Cmw', {id: 'satellite-streets-v10'});
		map.addLayer(satellite);
		rue   = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9nbml0ZXRlIiwiYSI6ImNqZHQ5N3NyOTBtbDEyd3Qzc21zbGczZGsifQ.fZl8Ld3MTbGPMlG-bB-Cmw', {id: 'streets-v10'});
		map.addLayer(rue);
		sombre   = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/dark-v9/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidG9nbml0ZXRlIiwiYSI6ImNqZHQ5N3NyOTBtbDEyd3Qzc21zbGczZGsifQ.fZl8Ld3MTbGPMlG-bB-Cmw', {id: 'dark-v9',});
		map.addLayer(sombre);
		
		
		

		
															
		// création d'une couche geoJson qui appelle le fichier "cinema.geojson"													
		
		function base1(feature,layer){
			layer.bindPopup();
		}
		L.geoJson(cour,{
			onEachFeature:base1
		}).addTo(map);


	function base(feature,layer){
		layer.bindPopup("<p>Nom:<i style='color:blue'>"+feature.properties.PPPTNAME+"</i></p><p>latitude: <i style='color:blue'>"+feature.properties.Y_LATITUD+"</i></p><p> longitude: <i style='color:blue'>"+feature.properties.X_LONGITUD+"</i></p></p><p> type de place: <i style='color:blue'>"+feature.properties.Place_type+"</i></p>");
	}
	L.geoJson(pop,{
		onEachFeature:base
	}).addTo(map);


		// fon ction pointToLayer qui ajoute la couche "cinema" à la carte, selon la symbologie "iconeCinema", et paramètre la popup
														
		// création d'un contrôle des couches pour modifier les couches de fond de plan	
		var baseLayers = {
			
			"satellite":satellite,
			"rue":rue,
			"sombre":sombre


		};
	
		L.control.layers(baseLayers).addTo(map);
}